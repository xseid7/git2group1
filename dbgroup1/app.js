const express=require('express');
const body =require('body-parser');
const cookie= require('cookie-parser');
const session= require('express-session');
const mysql=require('mysql');
const connection=require('express-myconnection')
const app= express();

app.use(express.static('public'));
app.set('view engine','ejs');
app.set('views','views');

app.use(body.urlencoded({extended: true}));
app.use(cookie());
app.use(session({secret:'Passw0rd',
resave: true,
saveUninitialized: true
}));
app.use(connection(mysql,{
  host:'localhost',
  user:'root',
  password:'Passw0rd',
  port:3306,
  database:'dbgroup1'
},'single'));

const s6023Route=require('./routes/s6023Route');
app.use('/',s6023Route);
const s6123Route=require('./routes/s611998023Route');
app.use('/',s6123Route);
const s6112Route=require('./routes/s611998012route');
app.use('/',s6112Route);
const s6106Route=require('./routes/s611998006Route');
app.use('/',s6106Route);

app.listen('8081');
