const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM tb23',(err,s6123e) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998023/s6123',{session: req.session,data:s6123e});
        });
    }); 
};

controller.add = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998023/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO tb23 set ?',[data],(err,s6123e)=>{
            res.redirect('/s611998023');
            });
        });
    };
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM tb23 WHERE id= ?',[id],(err,s6123e)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998023/s611998023Delete',{session: req.session,data:s6123e[0]});
        });
    });
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM tb23 WHERE id= ?',[id],(err,s6123e)=>{
            if(err){
                res.json(err);
            }
            console.log(s6123e);
            res.redirect('/s611998023');
        });
    });
};

controller.edit = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM tb23 WHERE id= ?',[id],(err,s6123e)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998023/s611998023Form',{session: req.session,data:s6123e[0]});
        });
    });
};

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM tb23 WHERE id= ?',[id],(err,s6123e)=>{
                    if(err){
                        res.json(err);
                    }
                    res.render('s611998023/s611998023Form',{session: req.session,data:s6123e[0]});
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  tb23 SET ?  WHERE id = ?',[data,id],(err,s6123e) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/s611998023');
               });
             });
        }
}

controller.new = (req,res) => {
    const data = null;
        res.render('s611998023/s611998023Form',{session: req.session,data:data});
};


module.exports = controller;