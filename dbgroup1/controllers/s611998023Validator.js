const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.addValidator = [
check('a611998023',"ชื่อผู้ใช้ไม่ถูกต้อง !").not().isEmpty(),
check('b611998023',"เลขผู้ใช้ไม่ถูกต้อง !").isFloat(),
check('c611998023',"รหัสไม่ถูกต้อง !").isBoolean(),
check('d611998023',"วันที่ไม่ถูกต้อง !").isDate()];

exports.editValidator = [
    check('a611998023',"ชื่อผู้ใช้ไม่ถูกต้อง !").not().isEmpty(),
    check('b611998023',"เลขผู้ใช้ไม่ถูกต้อง !").isFloat(),
    check('c611998023',"รหัสไม่ถูกต้อง !").isBoolean(),
    check('d611998023',"วันที่ไม่ถูกต้อง !").isDate()];