const controller = {};

controller.list = (req,res) => {
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM tb06',(err,s6106) =>{
      if(err){
        res.json(err);
      }
      res.render('s611998006/s611998006',{
        data:s6106
      });
    });
  });
};

controller.save = (req,res) => {
  const data = req.body;
  req.getConnection((err,conn) =>{
    conn.query('INSERT INTO tb06 set ?',[data],(err,s6106) =>{
      if(err){
        res.json(err);
      }
      console.log(s6106);
      res.redirect('/s611998006')
    });
  });
};

controller.delete = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('DELETE FROM tb06 WHERE id = ?',[id],(err,s6106) =>{
      if(err){
        res.json(err);
      }
      console.log(s6106);
      res.redirect('/s611998006');
    });
  });
};

controller.edit = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM tb06 WHERE id = ?',[id],(err,s6106) =>{
      if(err){
        res.json(err);
      }
      res.render('s611998006/s611998006Form',{
        data:s6106[0]
      });
    });
  });
};

controller.update = (req,res) => {
  const {id} = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err,conn) =>{
    conn.query('UPDATE tb06 SET ? WHERE id = ?',[data,id],(err,s6106) =>{
      if(err){
        res.json(err);
      }
      res.redirect('/s611998006');

      });
    });
  };

  controller.new = (req,res) => {
    const data = null;
    res.render('s611998006/s611998006Form',{
      data:data
    });
  };


module.exports = controller;
