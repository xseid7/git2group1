const controller ={};

controller.list=(req,res) => {
  req.getConnection((err,conn) =>{
    conn.query('select id,a23,b23,c23,DATE_FORMAT(d23,"%d-%M-%Y") as d23 from tb6023',(err,s6023s) =>{
      if (err) {
        res.json(err);
      }
      //res.json(s6023s);
      res.render('6023/s6023s',{ //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
        data:s6023s
      });
    });
  });
};
controller.save=(req,res)=>{
const data=req.body;
req.getConnection((err,conn)=>{
  conn.query('insert into tb6023 set?',[data],(err,s6023s) =>{
    if (err) {
      res.json(err);
    }
    console.log(s6023s);
    res.redirect('/tb6023'); // save ใน customer ไปเลย
  });
});
};

controller.delete=(req,res)=>{
const { id }=req.params;
req.getConnection((err,conn)=>{
  conn.query('Delete from tb6023 where id= ?',[id],(err,s6023s) =>{
    if (err) {
      res.json(err);
    }
    console.log(s6023s);
    res.redirect('/tb6023');
  });
});
};

controller.edit=(req,res)=>{
const { id }=req.params;
req.getConnection((err,conn)=>{
  conn.query('Select * from tb6023 where id= ?',[id],(err,s6023s) =>{
    if (err) {
      res.json(err);
    }
    res.render('6023/s6023Form',{
      data:s6023s[0]
    });
  });
});
};

controller.update=(req,res)=>{
const { id } =req.params;
const data=req.body;
req.getConnection((err,conn)=>{
  conn.query('update tb6023 set ? where id= ?',[data,id],(err,s6023s) =>{
    if (err) {
      res.json(err);
    }
    res.redirect('/tb6023');
    });
  });
};

controller.new =(req,res) => {
  const data=null;
  res.render('6023/s6023Form',{
    data:data
  });
};

module.exports = controller;
