
const controller = {};
controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query('select * FROM tb12',(err,s611998012data) => {
      if(err){
        res.json(err);
      }
    console.log(s611998012data);
    res.render('../views/s611998012s/s611998012data',{
      data:s611998012data
    });
    });
  });
};
controller.save = (req,res) => {
  //*********************************************************************************************************************
    // console.log(req.body);
    // res.send('Works OK');
    const data=req.body;
    req.getConnection((err,conn)=>{
        conn.query('INSERT INTO tb12 set ?',[data],(err,s611998012data)=>{
            if(err){
                res.json(err);
            }
            console.log('../views/s611998012s/s611998012data');
            res.redirect('/arm');
        });
    });
};
//*****************************************************************************************************************************
controller.delete = (req,res) => {
    const { id } = req.params;
      req.getConnection((err,conn) => {
        conn.query('DELETE FROM tb12  WHERE id = ?',[id],(err,s611998012data) => {
          if(err){
              res.json(err);
          }
          console.log('../views/s611998012s/s611998012data');
          res.redirect('/arm');
       });
    });
 };
 controller.edit = (req,res) => {
     const { id } = req.params;
       req.getConnection((err,conn) => {
         conn.query('SELECT * FROM tb12  WHERE id = ?',[id],(err,s611998012data) => {
           if(err){
               res.json(err);
           }
        res.render('../views/s611998012s/s611998012form',{
          data:s611998012data[0]
        });
      });
    });
  };

  controller.update = (req,res) => {
      const { id } = req.params;
      const data =req.body;
        req.getConnection((err,conn) => {
          conn.query('UPDATE  tb12 SET ?  WHERE id = ?',[data,id],(err,s611998012data) => {
            if(err){
                res.json(err);
            }
         res.redirect('/arm');
         });
       });
   };
   controller.new = (req,res) => {
       const data =null;
        res.render('../views/s611998012s/s611998012form',{
          data : data
        });
        };
module.exports = controller;
