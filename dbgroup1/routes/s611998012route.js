
const express = require('express');
const router = express.Router();
const s611998012Controller = require('../controllers/s611998012Controller');
router.get('/arm',s611998012Controller.list);
router.post('/arm/add',s611998012Controller.save);
router.get('/arm/delete/:id',s611998012Controller.delete);
router.get('/arm/update/:id',s611998012Controller.edit);
router.post('/arm/update/:id',s611998012Controller.update);
router.get('/arm/new',s611998012Controller.new);
module.exports = router;
