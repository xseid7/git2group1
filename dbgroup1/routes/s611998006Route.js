const express = require('express');
const router = express.Router();

const s611998006Controller = require('../controllers/s611998006Controller');
router.get('/s611998006',s611998006Controller.list);
router.post('/s611998006/add',s611998006Controller.save);
router.get('/s611998006/delete/:id',s611998006Controller.delete);
router.get('/s611998006/update/:id',s611998006Controller.edit);
router.post('/s611998006/update/:id',s611998006Controller.update);
router.get('/s611998006/new',s611998006Controller.new);

module.exports = router;
