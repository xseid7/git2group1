const express=require('express');
const router=express.Router();

const s6023Controller =require("../controllers/s6023Controller");
router.get('/tb6023',s6023Controller.list);
router.post('/tb6023/add',s6023Controller.save);
router.get('/tb6023/delete/:id',s6023Controller.delete);
router.get('/tb6023/update/:id',s6023Controller.edit);
router.post('/tb6023/update/:id',s6023Controller.update);
router.get('/tb6023/new',s6023Controller.new);

module.exports=router;
