const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.addValidator = [check('name',"ชื่อผู้ใช้ไม่ถูกต้อง !").not().isEmpty(),check('email',"Email ไม่ถูกต้อง !").isEmail()];
